package com.example.geoquiz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CheatActivity extends AppCompatActivity {

    private static final String EXTRA_ANSWER_IS_TRUE =
            "com.example.android.geoquiz.answer_is_true";     /* це просто назва ключа, вона може бути будь якою */
    private static final String EXTRA_ANSWER_SHOWN =
            "com.bignerdranch.android.geoquiz.answer_shown";
    private final String ANSWER_IS_SHOWS = "answer_is_shows";
    private boolean mAnswerIsShows = false;
    private boolean mAnswerIsTrue;
    private TextView mAnswerTextView;
    private Button mShowAnswerButton;
    private TextView mShow_API_Level;


    public static Intent newIntent(Context packageContext, boolean answerIsTrue) {     /* створює новий інтент, вигідно тим що цей метод можна викликати з різних місць */
        Intent intent = new Intent(packageContext, CheatActivity.class);
        intent.putExtra(EXTRA_ANSWER_IS_TRUE, answerIsTrue);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_in);
        LinearLayout linearLayout = findViewById(R.id.activity_cheat_linear_layout);
        linearLayout.startAnimation(animation);


        if (savedInstanceState != null) {
            if (savedInstanceState.getBoolean(ANSWER_IS_SHOWS)) {
                mAnswerIsShows = savedInstanceState.getBoolean(ANSWER_IS_SHOWS);
                setAnswerShownResult(true);
            }
        }
/*
        // Check if we're running on Android 5.0 or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Apply activity transition
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);                             //   Анімація

        } else {
            // Swap without transition
            System.out.println("Android version is lower 5.0");
        }
*/
        mAnswerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);
        /*int as = getIntent().getIntExtra("currentIndex", 0);*/            /* Отримуєм значення з Інтент по нейму */

        mAnswerTextView = (TextView) findViewById(R.id.answer_text_view);
        mShowAnswerButton = (Button) findViewById(R.id.show_answer_button);
        mShowAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAnswerIsTrue) {
                    mAnswerTextView.setText(R.string.true_button);
                } else {
                    mAnswerTextView.setText(R.string.false_button);
                }
                mAnswerIsShows = true;
                setAnswerShownResult(true);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int cx = mShowAnswerButton.getWidth() / 2;
                int cy = mShowAnswerButton.getHeight() / 2;
                float radius = mShowAnswerButton.getWidth();
                Animator anim = ViewAnimationUtils.createCircularReveal(mShowAnswerButton, cx, cy, radius, 0);  /* створюється анімація */
                anim.addListener(new AnimatorListenerAdapter() {                                                         /* додається прослуховувач який слуха коли анімація завершиться */
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mShowAnswerButton.setVisibility(View.INVISIBLE);
                    }
                });
                anim.start();                                                                                             /* анімація стартує */
                } else {
                    mShowAnswerButton.setVisibility(View.INVISIBLE);
                }
            }
        });

        mShow_API_Level = findViewById(R.id.show_api_level_textviev);
        mShow_API_Level.setHint("API Level " + Build.VERSION.SDK_INT);
    }

    private void setAnswerShownResult(boolean isAnswerShown) {
        Intent data = new Intent();
        data.putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown);
        setResult(RESULT_OK, data);
    }

    public static boolean wasAnswerShown(Intent result) {
        return result.getBooleanExtra(EXTRA_ANSWER_SHOWN, false);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(ANSWER_IS_SHOWS, mAnswerIsShows);
    }
}
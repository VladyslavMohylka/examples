package com.example.geoquiz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/* Controller*/
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";   /*константа для відправки повідомлень, щоб знати з якого класу*/
    private static final String KEY_INDEX = "index";    /* ключ по якому кладеться значення в методі savedInstanceState.put і дістається в методі savedInstanceState.get */
    private static final String KEY_IS_CHEATER = "cheater";
    private static final String KEY_HINTS_QUANTITY = "hints";
    private static final int REQUEST_CODE_CHEAT = 0;    /* якщо очікується результат від декількох активностей, то при виклику кожної активності їй передається свій код
                                                            щоб аотім знати від кого прийшла відповідь */

    private Button mTrueButton;
    private Button mFalseButton;
    private ImageButton mNextButton;
    private ImageButton mPrevButton;
    private TextView mQuestionTextView;
    private TextView mHintQuantityTextView;
    private Button mCheatButton;
    private Question[] mQuestionBank = new Question[]{
            new Question(R.string.question_australia, true),
            new Question(R.string.question_oceans, true),
            new Question(R.string.question_mideast, false),
            new Question(R.string.question_africa, false),
            new Question(R.string.question_americas, true),
            new Question(R.string.question_asia, true),
    };
    private boolean mIsCheater;
    private int mCurrentIndex = 0;
    private int mHintQuantity = 3;
    private int mRightAnswerQuantity = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate(Bundle) called");  /* відправля повідомдення в журнал */
        setContentView(R.layout.activity_main);      /* метод який заповня та виводить на екран віджети */

        if (savedInstanceState != null) {
            mCurrentIndex = savedInstanceState.getInt(KEY_INDEX, 0);
            mIsCheater = savedInstanceState.getBoolean(KEY_IS_CHEATER, false);
            mHintQuantity = savedInstanceState.getInt(KEY_HINTS_QUANTITY);
        }

        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);   /* посилання на заповнені віджети*/
        mQuestionTextView.setOnClickListener(v -> {                             /* слушателі для обробки взаємодії з користувачем*/
            mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
            updateQuestion();
        });

        mCheatButton = (Button) findViewById(R.id.cheat_button);
        mCheatButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*Intent intent = new Intent(MainActivity.this, CheatActivity.class);*/  /* адреса нової активності пакет і клас. Метод за замовчуванням */
                        /*intent.putExtra("currentIndex", mCurrentIndex);*/                      /* додавання доповнення в інтент, виймається методом getIntent().get"type"Extra() */
                        boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();
                        Intent intent = CheatActivity.newIntent(MainActivity.this, answerIsTrue); /* модифікований статичний метод */
                        /*startActivity(intent);     */                                                       /* <-- ПЕРЕХІД НА НОВУ АКТИВНІСТЬ*/
                        startActivityForResult(intent, REQUEST_CODE_CHEAT);

                    }
                }
        );


        mNextButton = (ImageButton) findViewById(R.id.next_button);
        mNextButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
                        mIsCheater = false;
                        updateQuestion();
                        trueFalseSetEnabled(true);
                        mNextButton.setEnabled(mCurrentIndex + 1 != mQuestionBank.length);
                        mPrevButton.setEnabled(true);
                    }
                }
        );

        mPrevButton = (ImageButton) findViewById(R.id.prev_button);
        mPrevButton.setOnClickListener(v -> {
            mCurrentIndex = (mCurrentIndex - 1) % mQuestionBank.length;
            updateQuestion();
            trueFalseSetEnabled(true);
            mPrevButton.setEnabled(mCurrentIndex != 0);
            mNextButton.setEnabled(true);
        });

        mTrueButton = (Button) findViewById(R.id.true_button);
        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                checkAnswer(true);
                trueFalseSetEnabled(false);
                result();
            }
        });

        mFalseButton = (Button) findViewById(R.id.false_button);
        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
                trueFalseSetEnabled(false);
                result();
            }
        });

        updateQuestion();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {    /* метод що викликається коли закривається якась активність і метод робить щось з результатом що верта активність */
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_CODE_CHEAT) {
            if (data == null) {
                return;
            }
            mIsCheater = CheatActivity.wasAnswerShown(data);
            if (CheatActivity.wasAnswerShown(data)) {
                --mHintQuantity;
                if (mHintQuantity == 0) {
                    mCheatButton.setEnabled(false);
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart() called");
        mHintQuantityTextView = (TextView) findViewById(R.id.hint_quantity_text_view);
        mHintQuantityTextView.setHint("Left " + mHintQuantity + " hints");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {                      /* викликається автоматично перед onPause() */
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState");
        savedInstanceState.putInt(KEY_INDEX, mCurrentIndex);
        savedInstanceState.putBoolean(KEY_IS_CHEATER, mIsCheater);
        savedInstanceState.putInt(KEY_HINTS_QUANTITY, mHintQuantity);
    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    private void updateQuestion() {
        int question = mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);
    }

    private void checkAnswer(boolean userPressedTrue) {
        boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();
        int messageResId = 0;
        if (mIsCheater) {
            messageResId = R.string.judgment_toast;
        } else {
            if (userPressedTrue == answerIsTrue) {
                messageResId = R.string.correct_toast;
                mRightAnswerQuantity++;
                System.out.println(mRightAnswerQuantity);
            } else {
                messageResId = R.string.incorrect_toast;
            }
        }
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT)
                .show();
    }

    private void trueFalseSetEnabled(boolean b) {
        mTrueButton.setEnabled(b);
        mFalseButton.setEnabled(b);
    }

    private void result() {
        if (mCurrentIndex + 1 == mQuestionBank.length) {
            float rightAnswerPercent = mRightAnswerQuantity * 100 / mQuestionBank.length ;
            System.out.println( mQuestionBank.length);
            String result = "You have " + rightAnswerPercent + " % right answers";
            mQuestionTextView.setText(result);
        }
    }
}
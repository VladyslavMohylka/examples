package classesForTests.car;

import iterationMethod.TypeCar;

import java.util.Objects;

public class Car {
    private final TypeCar typeCar;
    private Engine engine;
    private Transmission transmission;
    private final int maxSpeed;
    private final String model;

    public Car(TypeCar typeCar, Engine engine, Transmission transmission, int maxSpeed, String model) {
        this.typeCar = typeCar;
        this.engine = engine;
        this.transmission = transmission;
        this.maxSpeed = maxSpeed;
        this.model = model;
    }

    public TypeCar getTypeCar() {
        return typeCar;
    }

    public Engine getEngine() {
        return engine;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public String getModel() {
        return model;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return maxSpeed == car.maxSpeed &&
                typeCar == car.typeCar &&
                Objects.equals(engine, car.engine) &&
                Objects.equals(transmission, car.transmission) &&
                Objects.equals(model, car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeCar, engine, transmission, maxSpeed, model);
    }

    @Override
    public String toString() {
        return "{" +
                 typeCar +
                "," + engine +
                "," + transmission +
                "," + maxSpeed +
                "," + model +
                '}';
    }
}

package classesForTests.car;

public class Engine implements Comparable<Engine>{
    private final int hp;
    private final int square;
    private final String material;

    public Engine(int hp, int square, String material) {
        this.hp = hp;
        this.square = square;
        this.material = material;
    }

    @Override
    public int compareTo(Engine o) {
        return this.square - o.square;
    }

    @Override
    public String toString() {
        return "{" +
                "" + hp +
                "," + square +
                "," + material +
                '}';
    }

    @Override
    public boolean equals(Object o) {
       // if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Engine engine = (Engine) o;
        return false;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    public int getHp() {
        return hp;
    }
}

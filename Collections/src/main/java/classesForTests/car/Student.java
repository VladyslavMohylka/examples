package classesForTests.car;

public class Student {
    private final String name;
    private final String group;
    private final String course;
    private int mathematicsRate = 0;
    private int historyRate = 0;
    private int sportRate = 0;
    private int literatureRate = 0;
    private int philosophyRate = 0;

    public Student(String name, String group, String course) {
        this.name = name;
        this.group = group;
        this.course = course;
    }

    public Student(String name, String group, String course, int mathematicsRate, int historyRate,
                   int sportRate, int literatureRate, int philosophyRate) {
        this(name, group, course);
        this.mathematicsRate = mathematicsRate;
        this.historyRate = historyRate;
        this.sportRate = sportRate;
        this.literatureRate = literatureRate;
        this.philosophyRate = philosophyRate;
    }

    public String getName() {
        return name;
    }

    public String getGroup() {
        return group;
    }

    public String getCourse() {
        return course;
    }

    public int getMathematicsRate() {
        return mathematicsRate;
    }

    public int getHistoryRate() {
        return historyRate;
    }

    public int getSportRate() {
        return sportRate;
    }

    public int getLiteratureRate() {
        return literatureRate;
    }

    public int getPhilosophyRate() {
        return philosophyRate;
    }

    public void setMathematicsRate(int mathematicsRate) {
        this.mathematicsRate = mathematicsRate;
    }

    public void setHistoryRate(int historyRate) {
        this.historyRate = historyRate;
    }

    public void setSportRate(int sportRate) {
        this.sportRate = sportRate;
    }

    public void setLiteratureRate(int literatureRate) {
        this.literatureRate = literatureRate;
    }

    public void setPhilosophyRate(int philosophyRate) {
        this.philosophyRate = philosophyRate;
    }
}

package classesForTests.car;

public class Transmission {
    private final String system;
    private final int speed;

    public Transmission(String system, int speed) {
        this.system = system;
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "{" +
                "" + system +
                "," + speed +
                '}';
    }
}

package ex1;

import java.util.*;
                        /*Напишите метод, который на вход получает коллекцию объектов,
                            а возвращает коллекцию уже без дубликатов.*/

public class App {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(1);
        list.add(3);
        list.add(2);

        System.out.println(list);
        System.out.println(clearDuplicate(list));
    }

    private static <E> Set<E> clearDuplicate(Collection<E> collection) {
        return new HashSet<>(collection);
    }
}

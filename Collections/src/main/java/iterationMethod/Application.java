package iterationMethod;

import classesForTests.car.Car;
import classesForTests.car.Engine;
import classesForTests.car.Transmission;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Application {
    public static void main(String[] args) {
        Engine engine1 = new Engine(100, 1600, "Aluminium");
        Engine engine2 = new Engine(125, 1800, "Iron");
        Engine engine3 = new Engine(140, 2000, "Iron");
        Transmission transmission1 = new Transmission("Mechanic", 5);
        Transmission transmission2 = new Transmission("Automatic", 7);
        Transmission transmission3 = new Transmission("Mechanics", 6);
        Car car1 = new Car(TypeCar.MINIVAN, engine2, transmission1, 180, "Ford");
        Car car2 = new Car(TypeCar.CABRIOLET, engine3, transmission2, 200, "Chevrolet");
        Car car3 = new Car(TypeCar.SEDAN, engine1, transmission3, 170, "Lada");
        Car car4 = new Car(TypeCar.MINIVAN, engine2, transmission1, 180, "Volkswagen");
        Map<Engine, Car> map = new TreeMap<>();
        Map<Engine, Car> map1 = new HashMap<>();

        map.put(engine2, car1);
        map.put(engine2, car2);             // заміня старе значення
        map.put(engine3, car3);
        map.put(engine1, car1);
        map.put(engine2, car3);             // заміня старе значення

        map1.put(engine2, car1);
        map1.put(engine2, car2);

//        for (Map.Entry<Engine, Car> entry : map.entrySet()) {
//            System.out.println(entry.getKey() + " : " + entry.getValue());
//        }

//        System.out.println(map1.entrySet());
//        System.out.println(map1.size());
//        System.out.println(map1.get(engine2));

        map = sort(map, e -> e.getValue().getTypeCar() == TypeCar.MINIVAN);  // вигідно сортувати

        System.out.println(map);
    }

    static Map<Engine, Car> sort(Map<Engine, Car> map, Sort s) {
        Map<Engine, Car> result = new HashMap<>();
        for (Map.Entry<Engine, Car> entry : map.entrySet()) {
            if(s.sort(entry)) {
                result.put(entry.getKey(), entry.getValue());
            }
        }

        return result;
    }



}

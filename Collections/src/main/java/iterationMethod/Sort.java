package iterationMethod;

import classesForTests.car.Car;
import classesForTests.car.Engine;

import java.util.Map;

public interface Sort {
    boolean sort(Map.Entry<Engine, Car> e);
}

package iterationMethod;

public enum TypeCar {
    SEDAN,
    HATCH,
    CABRIOLET,
    MINIVAN
}

package mnozhestva;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Application {
    public static void main(String[] args) {
        Set<Integer> setA = new HashSet<>(Arrays.asList(1,2,3,4,5,6,7,8,9));
        Set<Integer> setB = new HashSet<>(Arrays.asList(1,2,4,6,7,9));

        System.out.println(union(setA, setB));
        System.out.println(intersect(setA, setB));
    }

    private static Set<?> union(Set<?>... set) {
        Set<?> result = new HashSet<>(set[0]);

            for (int i = 1; i < set.length; i++) {
                result.removeAll(set[i]);
            }

        return result;
    }

    private static Set<?> intersect(Set<?>... set) {
        Set<?> result = new HashSet<>(set[0]);

        for (int i = 1; i < set.length; i++) {
            result.retainAll(set[i]);
        }

        return result;
    }
}
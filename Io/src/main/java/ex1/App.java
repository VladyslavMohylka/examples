package ex1;

import javax.xml.crypto.Data;
import java.io.*;
import java.util.Date;

/*Написать программу, которая создаст текстовый файл и запишет в
    него список файлов (путь, имя, дата создания) из заданного
    каталога.*/
public class App {
    public static void main(String[] args) {
        String directoryPath = "Io\\src\\main\\java\\test_folder";
        String directoryResult = "Io\\src\\main\\java\\ex1\\result.txt";

        try (Writer writer = new FileWriter(directoryResult)) {


            getFileName(new File(directoryPath), writer);
        } catch (IOException e) {

        }
    }
    private static void getFileName(File file, Writer writer) throws IOException {
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                getFileName(f, writer);
            }
        } else {
            writer.write("- " + file.getPath().substring(29) + ", last modified: " + new Date(file.lastModified()) + "\n");
        }
    }
}

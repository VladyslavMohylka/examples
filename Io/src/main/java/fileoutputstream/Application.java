package fileoutputstream;

import java.io.*;
import java.util.Scanner;
import java.util.function.BinaryOperator;

public class Application {
    public static void main(String[] args) throws IOException {
//        String test = "Hello, how do yoy do?";
//        String outputLocation = "Io\\src\\main\\java\\fileoutputstream\\text_file_1.txt";// за замовчуванням
//        // створюється в папці C:\Users\Vladi\IdeaProjects\Examples
//        String inputLocation = "Io\\src\\main\\java\\fileoutputstream\\text_file_1.txt";
//
//        try (FileOutputStream fos = new FileOutputStream(outputLocation);  // try with resources
//             InputStream inn = new FileInputStream(inputLocation)) {
//            fos.write(test.getBytes());
//
//            while (true) {
//                int b = inn.read();
//                if (b == -1) {
//                    break;
//                } else {
//                    System.out.print((char)b);
//                }
//            }
//        }

//        Scanner in = new Scanner(System.in);
//        long a = in.nextLong();
//        long b = in.nextLong();
//
//
//
//
//        BinaryOperator<Long> rangeMultiplier = (Long x, Long y) -> {
//            long result = 1L;
//
//            for (long i = x; i <= y; i++) {
//                result *= i;
//            }
//
//            return result;
//        };
//        System.out.println(rangeMultiplier.apply(a,b));


        Scanner in = new Scanner(System.in);
        long a = in.nextLong();
        long b = in.nextLong();
        long x = in.nextLong();
        long y = in.nextLong();

        if (a == x) {
            System.out.println(true);
        } else if (b == y) {
            System.out.println(true);
        } else if (a + b == x + y) {
            System.out.println(true);
        } else {
            System.out.println(a - b == x - y && a + y == x + b);
        }


    }

}

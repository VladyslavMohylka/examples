package zip;

import java.io.*;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Application {
    public static void main(String[] args) {
        String inName = "Io\\src\\main\\java\\zip\\folder_to_zipping";      // з чого створюватимемо архів
        String outName = "Io\\src\\main\\java\\zip\\zip.zip";               // куди будем записувати
        File rootFile = new File(inName);                                   // створюєм каталог або файл по заданому шляху

        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(outName))) {
            for (File file : getListFiles(rootFile)) {                      // перебираєм список файлів в директорії
                FileInputStream fis = new FileInputStream(file);            // стрім звідки будем читать
                byte[] buffer = new byte[fis.available()];                  // буфер
                zos.putNextEntry(new ZipEntry(file.getName()));             // це щось на зразок заголовка файла, але без нього в архів файл не покладеться
                fis.read(buffer);
                zos.write(buffer);                                          // це тіло, без нього в архіві буде тільки пустий файл з назвою від ZipEntry
            }
            zos.closeEntry();
        } catch (IOException e) {
        }
    }

    public static ArrayList<File> getListFiles(File file) {                 // метод який поверта список файлів в директорії або один файл(якщо це файл)
        ArrayList<File> result = new ArrayList<>();

        if (file.isDirectory()) {                                           // перевіряєм чи це каталог
            for (File f : file.listFiles()) {                               // перебираєм вміст каталога
                result.addAll(getListFiles(f));                             // повторно викликаєм метод для кожного елемента в каталозі(рекурсія)
            }
        } else {                                                            // якщо це файл
            result.add(file);
        }

        return result;
    }
}

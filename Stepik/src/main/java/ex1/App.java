package ex1;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Compare compare = new Compare();
        System.out.println("enter number value: ");
        int valueNumbers = scanner.nextInt();

        for (int i = 0; i < valueNumbers; i++) {
            System.out.print("enter value: ");
            compare.compare(scanner.nextInt());
        }

        System.out.println(compare.perfect);
        System.out.println(compare.larger);
        System.out.println(compare.smaller);
    }
}

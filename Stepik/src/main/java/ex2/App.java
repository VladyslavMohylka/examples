package ex2;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        int value;
        do {
             value = scanner.nextInt();
             sum += value;
        } while (value != 0);

        System.out.println(sum);
        try (
                FileInputStream fis = new FileInputStream("Io\\src\\main\\java\\zip\\folder_to_zipping\\inner_folder\\pc.jpg");
                ByteArrayOutputStream baos = new ByteArrayOutputStream(fis.available())) {
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            baos.write(buffer);
            System.out.println(Arrays.toString( baos.toByteArray()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
